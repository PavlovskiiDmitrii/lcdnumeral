import React from 'react';
import PropTypes from 'prop-types';

import './Stick.scss'

const Stick = ({numPosition, activate}) => {
    return ( 
        <div className={`stick stick__${numPosition} ${activate ? 'stick_activate' : ''}`}>
            
        </div>
     );
}

Stick.propTypes = {
};

Stick.defaultProps = {
};
 
export default Stick;