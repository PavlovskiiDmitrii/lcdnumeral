import React from 'react';
import PropTypes from 'prop-types';
import LcdNumerical from '../LcdNumerical/LcdNumerical'

import './App.scss'

const App = () => {
    return ( 
        <div style={{
            width: '100px',
            height: '200px'
        }}>
            <LcdNumerical num={0}/>
        </div>
     );
}

App.propTypes = {
};

App.defaultProps = {
};
 
export default App;