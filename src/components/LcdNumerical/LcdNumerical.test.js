import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, render, configure  } from 'enzyme';
import LcdNumerical from './LcdNumerical';

configure({adapter: new Adapter()});


describe('<LcdNumerical />', () => {

  it('should render component ', () => {
    const component = shallow(<LcdNumerical/>);
    const wrapper = component.find(".lcdNumerical");
    expect(wrapper.length).toBe(1);
  });  

  //Snapshot
  it('should render component Snapshot', () => {
    const component = render(<LcdNumerical/>);
    expect(component).toMatchSnapshot();
  });
});