import React, { useEffect, useState } from 'react';
import Stick from '../Stick/Stick';

import PropTypes from 'prop-types';

import './LcdNumerical.scss'

const LcdNumerical = ({num}) => {
    
    const [arrActivate, setArr] = useState([]);
    const [thisNum, setThisNum] = useState(null);
    const numArr = [1,2,3,4,5,6,7];

    useEffect(() => {
        setThisNum(num);
    }, [])

    useEffect(() => {
        if (thisNum !== null) {
            createArr(thisNum);
        }
    }, [thisNum])

    const createArr = (num) => {
        switch(num) {
            case 0:
                setArr([1,2,3,5,6,7]);
                break;
            case 1:
                setArr([2,6]);
                break;
            case 2:
                setArr([1,2,4,5,7]);
                break;
            case 3:
                setArr([1,2,4,6,7]);
                break;
            case 4:
                setArr([2,3,4,6]);
                break;
            case 5:
                setArr([1,3,4,6,7]);
                break;
            case 6:
                setArr([1,3,4,5,6,7]);
                break;
            case 7:
                setArr([1,2,6]);
                break;
            case 8:
                setArr([1,2,3,4,5,6,7]);
                break;
            case 9:
                setArr([1,2,3,4,6,7]);
                break;
            default : 
                console.log('gg');
                break;
        }
    }

    return ( 
        <div className="lcdNumerical__wrap" onClick={() => {setThisNum(thisNum+1)}}>
            <div className="lcdNumerical">
                {
                    numArr.map((item, i) => (
                        <Stick
                            key={i}
                            activate={arrActivate.indexOf( item ) != -1 ? true : false}
                            numPosition={item}
                        />
                    ))
                }
            
                
                {/* <Stick numPosition="2"/>
                <Stick numPosition="3"/>
                <Stick numPosition="4"/>
                <Stick numPosition="5"/>
                <Stick numPosition="6"/>
                <Stick numPosition="7"/> */}
            </div>
        </div>
     );
}

LcdNumerical.propTypes = {
};

LcdNumerical.defaultProps = {
};
 
export default LcdNumerical;